const express = require('express')
require('../db/mongoose')
const router = new express.Router()
const auth = require('../middleware/auth')
const taskModel = require('../models/Tasks')

router.post('/tasks', auth, async (req, res) => {
    try {
        //const task = new taskModel(req.body)
        const task = new taskModel({ ...req.body, owner: req.user.id })

        await task.save()
        res.status(201).send()
    } catch (error) {
        res.status(400).send(error)
    }

    // task.save().then(() => {
    //     res.status(201).send(task)
    // }).catch((error) => {
    //     res.status(400).send(error)
    // })
})

router.patch('/tasks/:id', auth, async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['completed', 'description']
    const isValidOperation = updates.every(update => allowedUpdates.includes(update))

    try {
        if (!isValidOperation) {
            res.status(400).send({ error: 'Invalid Updates!' })
        }

        const task = await taskModel.findOne({ _id: req.params.id, owner: req.user._id })

        if (!task) {
            return res.status(404).send({ error: 'Task not found!!!' })
        }

        updates.forEach(update => task[update] = req.body[update])
        await task.save()

        res.send(task)
    } catch (error) {
        res.status(400).send(error)
    }

})

// Get /tasks?completed=true
// Get /tasks?limit=10&skip=20
// Get /tasks?sortBy=created_asc
router.get('/tasks', auth, async (req, res) => {
    const match = {}
    const sort = {}

    if (req.query.completed) {
        match.completed = req.query.completed === 'true'
    }

    if (req.query.sortBy) {
        const parts = req.query.sortBy.split('_')
        sort[parts[0]] = parts[1] === 'desc' ? -1 : 1
    }

    try {
        await req.user.populate({
            path: 'tasks',
            match,
            options: {
                limit: parseInt(req.query.limit),
                skip: parseInt(req.query.skip),
                sort
            }
        }).execPopulate()
        res.send(req.user.tasks)
    } catch (error) {
        res.status(500).send(error)
    }
})

router.get('/tasks/:id', auth, async (req, res) => {
    const _id = req.params.id
    console.log('Req Parms: ', req)

    try {
        // const task = await taskModel.findById(_id)
        const task = await taskModel.findOne({ _id, owner: req.user._id })

        if (!task) {
            return res.status(404).send()
        }

        res.status(200).send(task)
    } catch (error) {
        res.status(404).send(error)
    }
})

router.delete('/tasks/:id', auth, async (req, res) => {
    try {
        const task = await taskModel.findOneAndDelete({ _id: req.params.id, owner: req.user._id })

        if (!task) {
            return res.status(404).send({ error: 'Task Not Found!!!' })
        }

        res.send(task)
    } catch (error) {
        res.status(400).send(error)
    }
})

module.exports = router
