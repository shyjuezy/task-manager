const sgMail = require('@sendgrid/mail')

sgMail.setApiKey(process.env.SENDGRID_API_KEY)

const sendWelcomeEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'shyju.viswambaran@dxc.com',
        subject: 'Thanks for joining the community',
        text: `Welcome to the App, ${name}. Let me know how you get along with the app`
    })
}

const sendUserDelete = (email, name) => {
    sgMail.send({
        to: email,
        from: 'shyju.viswambaran@dxc.com',
        subject: 'Sorry to see you leave',
        text: `Hello, ${name}, we are really sorry to see you leave the app. Please reach out to us if we could have done anything better to keep you`
    })
}

module.exports = {
    sendWelcomeEmail,
    sendUserDelete
}